package com.example.skindemo.activity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skindemo.R;
import com.example.skindemo.adapter.SkinAdapter;

/**
 * @ClassName: RecyclerViewActivity
 * @Author: 史大拿
 * @CreateDate: 1/4/23$ 3:58 PM$
 * TODO
 */
public class RecyclerViewActivity extends SkinBaseActivity {

    @Override
    protected void initCreate(Bundle savedInstanceState) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new SkinAdapter());
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_recycler_view;
    }
}
